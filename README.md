"""<br>
Purpose: This test is designed to test the DevOps candidates’s scripting skills<br>
Use SaltStack or Ansible to automate the process of creating an AWS EC2 instance and complete the following tasks.<br>
    1. Provision a t2.micro instance, with an OS of your own choice.<br>
    2. Change the security group of of the instance to ensure it’s security level.<br>
    3. Change the OS/Firewall settings of the started instance to further enhance it’s security level.<br>
    4. Install Docker CE.<br>
    5. Deploy and start an nginx container in docker.<br>
    6. Run a command to test the healthiness of the nginx container.<br>
    7. Fetch the output of the nginx container’s default http page.<br>
    8. Stipe all the html tags, grep and count all the words in the fetched html, then print the result in the following manner, sort the result in alphabet order<br>
    9. Logs the resource usage of the container every 10 seconds.<br>
"""<br>

I haven't done step2 since it's really useless from my point of view. Not use seperate var files since simple purpose.<br>

Time spent for these tasks: 1.5 hours <br>

Reference: [Ansible doc](https://docs.ansible.com/ansible/latest/index.html)

# Description:<br>
1. ec2.yml is the playbook to launch a clean Ubuntu 16.04 ec2 with sg<br>
2. docker-web.yml is the playbook to install docker ce and install nginx container and check nginx container status<br>
3. post-check.yml is the playbook to fetch nginx default page and grep non-html content then sort words by alphabet, log the resource usage<br>
<br>

# Requirements:<br>
1. ansible with its necessary modules<br>
2. Pre-create key pair for launch ec2 and pre-config credentials to run ec2.yml<br>
<br>

# Comments:<br>
From my point of view, it's very not so good to run these tasks with only ansible or salt. It will bring lots of issues in the future, hard to maintain and hard to deploy,really not so good to do like this.
<br>
If it's me to work on these projects. I will do the following:<br>
1. Use terraform to create related IAM users, roles, VPC with its basic setttings, prepare key-pair and sg rules.<br>
2. Write a bootstrap scrit to set OS firewall, install docker ce with specific version and nginx with specific version. (BTW, Step2 is useless, I haven't see anyone set OS firewall with SG worked)\<br>
3. Launch a clean ec2 instance manually and configure user-data to run bootstrap script.<br>
4. Check nginx container healthiness and do other steps.<br>
5. Stop instance and use packer to build private image for future usage.<br>
<br>

List some of my comments under each step<br>
1. Provision a t2.micro instance, with an OS of your own choice.<br>
Q: Is this instance under classic network or under VPC? What's kind of backend storage? Any user data? Spot instance? etc.......<br> 
Toooooooo many unknows for me. So actually it's impossible to launch one without any information.<br>
2. Change the security group of of the instance to ensure it’s security level.<br>
Q: What's the usage of the server? How is it possible to limit network without even know the usage of the server or any ports will be listend to? And what's the defination of the security? SSH security? Or App level security? For me, this task is also impossible to work on.<br> 
3. Change the OS/Firewall settings of the started instance to further enhance it’s security level.<br>
Q: I haven't done this task since it's really so bad to set another layer of OS firewall with security group already work.
4. Install Docker CE.\<br>
Q: Version? No version control is really not good.<br>
5. Deploy and start an nginx container in docker.<br>
Q: Again, any available nginx image? Version? <br>
6. Run a command to test the healthiness of the nginx container.<br>
Q: Does it mean we can consider healthy is nginx container is healthy? Or port is open? Or no error in log?
7. Fetch the output of the nginx container’s default http page.<br>
8. Stipe all the html tags, grep and count all the words in the fetched html, then print the result in the following manner, sort the result in alphabet order<br>
9. Logs the resource usage of the container every 10 seconds.<br>
Q: To where?<br>